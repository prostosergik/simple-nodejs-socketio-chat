// Library dependencies
var mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities');

var connection = mysql.createConnection({
    host:     'localhost',
    user:     'root',
    // password: 'gfhfnhegth5188882',
    password: 'root',
    database: 'webrtc'
});

// Mix-in for Data Access Methods and SQL Autogenerating Methods
mysqlUtilities.upgrade(connection);

// Mix-in for Introspection Methods
mysqlUtilities.introspection(connection);


connection.connect(function(error){
    if(error){
        console.log("Error connecting to MySQL: " + error);
        return null;

    } else{
        console.log('Connected to mysql!');
        return this;
    }
});

exports.connection = connection;
