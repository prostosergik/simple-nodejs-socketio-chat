var express = require('express')
	, app = express()
	, http = require('http')
	, server = http.createServer(app)
	, mysql = require('./mysql.js')
	, md5 = require('MD5')
	, io = require('socket.io').listen(server)
	, _ = require('underscore')._;


var salt="HereIsMyVerySimpleSaltLine";


app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ipaddr', process.env.OPENSHIFT_NODEJS_IP || "localhost");

//app.use(express.json());
//app.use(express.urlencoded());
//app.use(express.methodOverride());

app.use(express.static(__dirname + '/public'));
app.use('/components', express.static(__dirname + '/components'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/icons', express.static(__dirname + '/icons'));
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);

server.listen(app.get('port'), app.get('ipaddr'), function(){
	console.log('Express server listening on  IP: ' + app.get('ipaddr') + ' and port ' + app.get('port'));
});

// routing
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/public/index.html');
});



var p = function(){
	var i;
    for (i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
}


//app state
var $state = {
	sockets: {},
	public_rooms: {
		'public': "Main chat",
		'work': "Work conversations",
		'travel': "Travel"
	},
	default_room: 'public'
};


//common events

var updateRooms = function(socket) {

	//get current user's private rooms

	mysql.connection.query(
		        "SELECT DISTINCT r.id, r.room_key, r.name, COUNT( * ) as num_users \
				FROM room_users ru \
				INNER JOIN rooms r ON ( ru.room_id = r.id ) \
				WHERE room_id \
				IN ( \
					SELECT room_id \
					FROM room_users \
					WHERE user_id = ? \
					GROUP BY room_id \
				) \
				GROUP BY ru.room_id", socket.user.id,
		        function(err, rows) {
		        	if(err) {
		        		// socket.emit('common_error', 'No rooms found.');
		        		p(err);
		        	} else {
		        		if(!rows) {
		        			p('No rooms found.');
		        			// socket.emit('login_error');
		        		} else {
		        			$state.sockets[socket.id].rooms = rows;
		        		}
						socket.emit('update_rooms', $state.sockets[socket.id].rooms, $state.sockets[socket.id].room)
		        	}
		        });


}



var updateUsers = function(curr_user, what_user_did){
  	//1) for each online socket get users friends
  	//2) get online users array
  	//3) Send to given socket list of friends in format user, satatus

	users_online = {};
	_.each($state.sockets, function(eachSocket){
		if(eachSocket.user) {
			users_online[eachSocket.user.id] = true;
		}
	});


	_.each($state.sockets, function(eachSocket){

		if(eachSocket.user) {

			var friends_online = {}; //save iterator's online friends here
			var friends_ids = {};    //save iterator's friends IDs for fast check if we can send him a message

			//select FRIENDS online for security reasons.
			_.each(eachSocket.user.friends, function(eachFriend){
				if(users_online[eachFriend.id]) {
					friends_online[eachFriend.id] = true;
				}

				friends_ids[eachFriend.id] = true;
			})

			eachSocket.emit('update_users', eachSocket.user.friends, friends_online);

			//you
			if(eachSocket.user.id == curr_user.id && what_user_did == 'login') {
				eachSocket.emit('update_chat', [{name: 'SERVER',  message: 'You are now connected.'}]);
			} else { //not you

				//check, is iterator a friend? We can tell only that friend enters.
				if(friends_ids[curr_user.id]) {
					if(what_user_did == 'login') {
						eachSocket.emit('update_chat', [{name: 'SERVER',  message: curr_user.name + ' joined the chat.'}]);
					}

					if(what_user_did == 'logout') {
						eachSocket.emit('update_chat', [{name: 'SERVER',  message: curr_user.name + ' leave the chat.'}]);
					}

					if(what_user_did == 'disconnected') {
						eachSocket.emit('update_chat', [{name: 'SERVER',  message: curr_user.name + ' was disconnected.'}]);
					}
				}


			}

		}
	});





}



var updateUsers_old = function(socket) {

	if(!$state.sockets[socket.id]) return;

	var friends_online = {};
	//TODO: now it shows all users online, whicj is inscure. Need to check if user is friend and then set him to online.
	_.each($state.sockets, function(eachSocket){
		if(eachSocket.user) {
			friends_online[eachSocket.user.id] = true;
		}
	});

	//TODO: sam way add user device, if required. Im not sure is that required or not...

	if(socket.user && socket.user.id) {

		//save some db traffic by caching friends list. Next time friends will be updated on login.
		if(typeof($state.sockets[socket.id].user.friends) == 'undefined')  {

			mysql.connection.query(
				        'SELECT u.id, u.name FROM users u INNER JOIN friends f ON(u.id = f.friend_id) WHERE f.user_id=?', socket.user.id,
				        function(err, rows) {
				        	if(err) {
				        		// socket.emit('common_error');
				        		p(err);
				        	} else {
				        		if(!rows) {
				        			p('No friends found');
				        		} else {
				        			$state.sockets[socket.id].user.friends = rows;
									socket.emit('update_users', $state.sockets[socket.id].user.friends, friends_online);
				        		}
				        	}
				        });
		} else {
			socket.emit('update_users', $state.sockets[socket.id].user.friends, friends_online);
		}

	}

}


var joinRoom = function(socket, room_key, server_message) {

	if(server_message) {
		socket.emit('update_chat', [{ name: 'SERVER', message: server_message}]);
	}

	socket.join(room_key);

	//get_history
	mysql.connection.query(
	        "SELECT u.name, message FROM history h \
			INNER JOIN rooms r ON ( r.id = h.room_id ) \
			INNER JOIN users u ON ( h.user_id = u.id ) \
			WHERE r.room_key = ? \
			ORDER BY TIMESTAMP DESC LIMIT 5",
			room_key,
	        function(err, rows) {
	        	if(err) throw err;

		        socket.emit('update_chat', rows.reverse());

	        });
}



io.sockets.on('connection', function (socket) {

    p('New connection established #'+socket.id);

    $state.sockets[socket.id] = socket; //will contain link to socket.


	socket.on('login', function(name, password, user_cookie, device){

		if(user_cookie) {
			var conditions = "password=?";
			var conditions_vars = user_cookie;
		} else {
			var conditions = "login=? AND password=?";
			var conditions_vars = [name, md5(password+salt)];
		}

		mysql.connection.queryRow(
		        'SELECT id, name, login, password FROM users WHERE '+conditions, conditions_vars,
		        function(err, row) {
		        	if(err) {
		        		socket.emit('login_error');
		        		p(err);
		        	} else {
		        		if(!row) {
		        			p('Wrong login atteppt.');
		        			socket.emit('login_error');
		        		} else {
		        			p('Logged new user ' + row.name);

		        			socket.emit('login_ok', row);

		        			socket.user = row;
		        			socket.user.device = device;

		        			//get user friends and save to object
		        			mysql.connection.query(
					        'SELECT u.id, u.name FROM users u INNER JOIN friends f ON(u.id = f.friend_id) WHERE f.user_id=?', socket.user.id,
					        function(err, rows) {
					        	if(err) {
					        		// socket.emit('common_error');
					        		p(err);
					        	} else {
					        		if(!rows) {
					        			p('No friends found');
					        		} else {
					        			$state.sockets[socket.id].user.friends = rows;
					        		}
		        					updateUsers(socket.user, 'login'); // will send whatever required for each active socket
					        	}
					        });


		        			updateRooms(socket);

							//socket.broadcast.emit('update_chat', [{name:'SERVER',  message: socket.user.name + ' joined the chat.'}]);
		        		}
		        	}
		        });
	});


	socket.on('start_chat', function(friend_id){

		mysql.connection.queryRow(
		        "SELECT r.id, r.name, r.room_key \
				FROM room_users ru \
				INNER JOIN rooms r ON ( r.id = ru.room_id ) \
				WHERE room_id \
				IN ( \
					SELECT room_id \
					FROM room_users ru_1 \
					INNER JOIN room_users ru_2 \
					USING ( room_id ) \
					WHERE ru_1.user_id = ? \
					AND ru_2.user_id = ? \
				) \
				GROUP BY room_id \
				HAVING COUNT( * ) = 2", [socket.user.id, friend_id],
		        function(err, row) {
		        	if(err) {
		        		// socket.emit('login_error');
		        		p(err);
		        	} else {
		        		if(!row) {
		        			p('Creating room for users #'+socket.user.id+' and #'+friend_id);

		        			//TODO: get users names for room name
		        			var room_name = "Room for users #"+socket.user.id+' and #'+friend_id; //TODO: add names.
		        			var room_key = "room_"+socket.user.id+'_'+friend_id;

		        			mysql.connection.query(
		        				"INSERT INTO rooms SET name=?, room_key=?, create_time = UNIX_TIMESTAMP(), owner_id=?",
		        				[room_name, room_key, socket.user.id],
		        				function(err, result) {
									if (err) throw err;

									//adding connection
									mysql.connection.query("INSERT INTO room_users SET room_id=?, user_id=?", [result.insertId, socket.user.id]);
									mysql.connection.query("INSERT INTO room_users SET room_id=?, user_id=?", [result.insertId, friend_id]);

									//joining room
		        					$state.sockets[socket.id].room = {
		        						name: room_name,
		        						room_key: room_key
		        					}; //what tha hell? socket.room have no effect!

		        					//join the room + show history + tell message
		        					joinRoom(socket, room_key, 'you have connected to "'+ room_name + '"');

		        					updateRooms(socket);
		        					p('User '+socket.user.name+' joined room '+ room_name);
									socket.broadcast.to(newroom).emit('update_chat', [{ name: 'SERVER', message: socket.user.name+' has joined this room'}]);
								});

		        		} else {
		        			//room found
		        			p('Room ' + row.name + ' exists for users #'+socket.user.id+' and #'+friend_id);

		        			$state.sockets[socket.id].room = row;

        					//join the room + show history + tell message
        					joinRoom(socket, row.room_key, 'you have connected to "'+ row.name + '"');

		        			//tell you where you just joined.
		        			p('User '+socket.user.name+' joined room '+ row.name);
							socket.broadcast.to(row.room_key).emit('update_chat', [{ name: 'SERVER', message: socket.user.name+' has joined this room'}]);

		        			updateRooms(socket);
		        		}
		        	}
		        });



	});

	socket.on('join_room', function(newroom){

		//search for room in DB
		mysql.connection.queryRow(
		        "SELECT r.id, r.name, r.room_key \
				FROM room_users ru \
				INNER JOIN rooms r ON ( r.id = ru.room_id ) \
				WHERE r.room_key = ? AND ru.user_id = ? ", //also check that user belongs to this room
				[newroom, socket.user.id],
		        function(err, row) {
		        	if(err) throw err;

	        		if(!row) {
	        			//can't!
	        			p('user trying to join room key '+newroom + ', but looks like this room not belongs to him or room not found.');

	        		} else {
	        			//room found
	        			p('Room ' + row.name + ' found');

						//leave old room, if joined.
						if($state.sockets[socket.id].room) {
							socket.leave($state.sockets[socket.id].room.room_key);
							// sent message to OLD room
							socket.broadcast.to($state.sockets[socket.id].room.room_key).emit('update_chat', [{name: 'SERVER', message: socket.user.name+' has left this room'}]);
						}


						//join the room + show history + tell message
    					joinRoom(socket, row.room_key, 'you have connected to "'+ row.name + '"');

						// update socket session room title
	        			$state.sockets[socket.id].room = row;
	        			updateRooms(socket);

	        			//tell room users that new user joined room
						socket.broadcast.to(newroom).emit('update_chat', [{ name: 'SERVER', message: socket.user.name+' has joined this room'}]);
	        			p('User '+socket.user.name+' joined room '+ row.name);
	        		}

		        });
	});

	// get available rooms for user
	socket.on('get_available_rooms', function(user_id){

		//search for room in DB
		mysql.connection.query(
		        "SELECT r.* FROM rooms r \
				RIGHT JOIN room_users ru ON ( r.id = ru.room_id ) \
				WHERE user_id = ? \
				AND room_id NOT IN ( \
					SELECT room_id FROM room_users WHERE user_id = ? \
				) ",
				[socket.user.id, user_id],
		        function(err, rows) {
		        	if(err) throw err;

	        		if(!rows || rows.length == 0) {
	        			socket.emit('set_available_rooms', false, user_id);
	        		} else {
						socket.emit('set_available_rooms', rows, user_id);
	        		}

		        });
	});


	socket.on('add_to_room', function(user_id, room_id){

		//TODO: think about security! User can add another user only to room where he allready added!
		//TODO: chack if valid user_id sent. Can be in insert query, probably?
		mysql.connection.queryRow(
		        "SELECT * FROM room_users  \
				WHERE user_id = ? \
				AND room_id = ?",
				[socket.user.id, room_id],
		        function(err, row) {
		        	if(err) throw err;

	        		if(!row) {
	        			p("User can't add someone to room where this user not in.")
	        			//socket.emit('set_available_rooms', false, user_id);
	        		} else {
	        			//add to room query
	        			mysql.connection.query("INSERT INTO room_users SET room_id=?, user_id=?",
	        									[room_id, user_id],
	        									function(err, row){
	        										if(!err) {
														updateRooms(socket);
														socket.emit('add_to_room_ok');
	        										}
	        									});
	        		}

		        });
	});


	// send message to room
	socket.on('send_chat', function (message) {

		//save history
		mysql.connection.query("INSERT INTO history SET room_id=?, user_id=?, message=?, timestamp=UNIX_TIMESTAMP()",
								[socket.room.id, socket.user.id, message]);

		//update chat window
		io.sockets.in($state.sockets[socket.id].room.room_key).emit('update_chat', [{name: socket.user.name, message: message}]);

		//TODO: here we can save last read message, dont know how, for now.
	});


	socket.on('logout', function(){

		var curr_user = _.clone(socket.user); //save socket, to real var that will be destroyed by garbage collector. TODO: chack!!!

		if(socket.room) {
			socket.leave(socket.room.room_key);
		}

		delete socket.user;
		delete socket.room;
		delete socket.friends;

		socket.emit('logout_ok');
		updateUsers(curr_user, 'logout');
	});


	// when the user disconnects.. perform this
	socket.on('disconnect', function(){

		if(socket.user) {
			var curr_user = _.clone(socket.user); //save socket, to real var that will be destroyed by garbage collector. TODO: chack!!!

			if(socket.room) {
				socket.leave(socket.room.room_key);
			}

			updateUsers(curr_user, 'disconnected');
		}

		delete $state.sockets[socket.id];
		delete socket.room;
		delete socket.user;
		delete socket.friends;
	});
});
