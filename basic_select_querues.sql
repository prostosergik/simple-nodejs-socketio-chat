SELECT *
FROM room_users ru_1
INNER JOIN room_users ru_2
USING ( room_id )
WHERE ru_1.user_id =1
AND ru_2.user_id =2

-- выбрать комнаты, в которых есть эта пара пользователей


SELECT room_id
FROM  `room_users`
WHERE 1
GROUP BY room_id
HAVING COUNT( * ) = 2

-- выбрать комнаты, где только 2 участника

SELECT room_id
FROM  `room_users`
WHERE room_id
IN (
	SELECT room_id
	FROM room_users ru_1
	INNER JOIN room_users ru_2
	USING ( room_id )
	WHERE ru_1.user_id = 1
	AND ru_2.user_id = 2
)
GROUP BY room_id
HAVING COUNT( * ) = 2

-- выбрать комнаты, где только 2 ЭТИХ участника.


SELECT r.id, r.room_key, r.name
FROM room_users ru
INNER JOIN rooms r ON ( ru.room_id = r.id )
WHERE ru.user_id = 1
GROUP BY ru.room_id

-- выбрать комнаты пользователя



SELECT DISTINCT r.id, r.room_key, r.name, COUNT( * )
FROM room_users ru
INNER JOIN rooms r ON ( ru.room_id = r.id )
WHERE room_id
IN (

	SELECT room_id
	FROM room_users
	WHERE user_id =1
	GROUP BY room_id
)
GROUP BY ru.room_id

-- выбрать все комнаты пользователя с кол-вом пользователей.

SELECT r.*
FROM rooms r
RIGHT JOIN room_users ru ON ( r.id = ru.room_id )
WHERE user_id =1
AND room_id NOT
IN (
	SELECT room_id
	FROM room_users
	WHERE user_id =3
)

-- выбрать все комнаты, доступные текущему пользователю 1 где еще нету пользователя 3


SELECT u.name, message
FROM history h
INNER JOIN rooms r ON ( r.id = h.room_id )
INNER JOIN users u ON ( h.user_id = u.id )
WHERE r.room_key = 'room_1_2'
ORDER BY TIMESTAMP DESC
LIMIT 5
