var p = function(){
	var i;
    for (i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
}

var socket = null;
var $state = {
	user: null,
	users: [],
	// room: []
};

//detect device
var device = "desktop";
if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
	device = "mobile";
}

$(function(){

	//DOM hacks
	$("form").submit(function(event) {
		event.preventDefault();
	});

	$("#conversation").bind("DOMSubtreeModified",function() {
		$("#conversation").animate({
			scrollTop: $("#conversation")[0].scrollHeight
		});
	});


	//socket.io init
	socket = io.connect("http://localhost:8080/");

	//autologin
	var user_cookie = $.cookie('chat-user');

	if(user_cookie) {
		//auto-login by hash.
		socket.emit("login", '', '', user_cookie, device); //TODO: remove device
	} else {
		$('#login-form').show();
	}

	//DOM events that send data to server

	//log in form submitted
	$(document).on('click', '#join', function(){
		var name = $("#name").val();
		var password = $("#password").val();

		if (name === "" || name.length < 1) {
			$("#errors").empty();
			$("#errors").append("Please enter a name");
			$("#errors").show();
		} else if (password === "" || password.length < 1) {
			$("#errors").empty();
			$("#errors").append("Please enter a password");
			$("#errors").show();
		} else {
			// p("login", name, password, device);
			socket.emit("login", name, password, false, device);
    	}

	});

	$(document).on('click', '#logout', function(){
		socket.emit('logout');
	});

	//start chat with selected PERSON
	$(document).on('click', 'a.start_chat', function() {
		socket.emit('start_chat', $(this).attr('data-user-id'));
	});


	//join selected ROOM
	$(document).on('click', 'a.join_room', function() {
		socket.emit('join_room', $(this).attr('data-room-key'));
	});

	//send message
	$(document).on('submit', '#message_form', function(){
		socket.emit('send_chat', $('#message').val());
		$("#message").val('');
	});

	$(document).on('click', 'a.add_to_room_init', function() {

		var user_id = $(this).attr('data-user-id');

		//clean list and show loader
		$('#available_rooms').empty();
		$('#add_user_to_room_modal div.no_rooms').hide();
		$('#add_user_to_room_modal div.loading').show();

		//request available rooms for user
		socket.emit('get_available_rooms', $(this).attr('data-user-id'));

		//show modal with loader
		$('#add_user_to_room_modal').modal();
	});

	$(document).on('click', 'a.add_to_room', function() {
		socket.emit('add_to_room', $(this).attr('data-user-id'), $(this).attr('data-room-id'));
	});


// ==========================================================================================================================================

	//Socket events that react to server response

	socket.on('login_ok', function(user){
		$('#login-form').hide();
		$('#main-chat-screen').show();
		$('#user_name').html(user.name);
		$("#msgs").empty();
		//save user hash in cookie
		$.cookie('chat-user', user.password);
		$state.user = user;
	});

	socket.on('login_error', function(user){
		$("#errors").empty();
		$("#errors").append("Wrong username and/or password.");
		$("#errors").show();
	});

	socket.on('logout_ok', function(user){
		$('#login-form').show();
		$('#main-chat-screen').hide();
		//remove login cookie
		$.removeCookie('chat-user');
	});


	socket.on('update_users', function(friends, friends_online){
		//update app state
		$state.friends = friends;

		//update DOM
		$("#friends").empty();
	    $('#friends').append("<li class=\"list-group-item active\">Your friends <span class=\"badge\">"+friends.length+"</span></li>");

	    $.each(friends, function(a, obj) {

	    	// p(friends_online);

			if(friends_online[obj.id]) {
				html = "<i>(online)</i> ";
			} else {
				html = "";
			}
			//TODO: use underscore templates here
			$('#friends').append('<li class="list-group-item">'
								  + '<span>' + obj.name + ' #' + obj.id +'</span> '
								  // + '<i class="fa fa-'+obj.device+'></i> '
								  + html
								  + ' <a class="fa fa-comment start_chat"    href="javascript:void(0);" data-user-id="'+ obj.id + '">&nbsp;</a>'
								  + ' <a class="fa fa-plus add_to_room_init" href="javascript:void(0);" data-user-id="'+ obj.id + '">&nbsp;</a>'
								);
	    });
	});

	socket.on('update_rooms', function(rooms, room){
		//update app state
		$state.room = room;
		$state.rooms = rooms;

		// p(rooms);

		if(room) {
			$('#current_room_name').show().html(room.name);
		} else {
			room = {room_key : null}; //to be able to compare with obj.room_key
		}

		//update DOM
		$("#rooms").empty();
	    $('#rooms').append('<li class="list-group-item active">Rooms you are joined <span class="badge" id="rooms_cnt"></span></li>');

		//update DOM
		$("#private_rooms").empty();
	    $('#private_rooms').append('<li class="list-group-item active">Private rooms <span class="badge" id="private_rooms_cnt"></span></li>');

	    var private_rooms_cnt = 0;
	    var rooms_cnt = 0;
	    $.each(rooms, function(a, obj) {
			//TODO: use underscore templates here

			if(obj.num_users == 2) { //private
				private_rooms_cnt ++;
				$('#private_rooms').append('<li class="list-group-item '+(room.room_key == obj.room_key ? 'active' : '')+'">'+
										'<span>' + obj.name + '</span> '+
										(room.room_key != obj.room_key ? '<a class="fa fa-comment join_room" href="javascript:void(0);" data-room-key="'+ obj.room_key + '">&nbsp;</a>':'')
								);
			} else {
				rooms_cnt ++;
				$('#rooms').append('<li class="list-group-item '+(room.room_key == obj.room_key ? 'active' : '')+'">'
										+ '<span>' + obj.name + '</span> '
										+ (room.room_key != obj.room_key ? '<a class="fa fa-comment join_room" href="javascript:void(0);" data-room-key="'+ obj.room_key + '">&nbsp;</a>':'')
										+ '<span class="badge">'+obj.num_users+'</span>'
									);
			}



	    });

	    //DIRTY HACK =(
	    $('#private_rooms_cnt').html(private_rooms_cnt);
	    $('#rooms_cnt').html(rooms_cnt);

	});

	socket.on('update_chat', function(update_data){
		$.each(update_data, function(key, data){
	    	$("#msgs").append("<li><strong><span class='text-success'>" + data.name + "</span></strong>: " + data.message + "</li>");
		});
	});

	//render available rooms into popup
	socket.on('set_available_rooms', function(rooms, user_id){

		$('#add_user_to_room_modal div.loading').hide();

		if(rooms === false) {
			//setup link
			$('#add_user_to_room_modal div.no_rooms a.start_chat').attr('data-user-id', user_id);
			//show message
			$('#add_user_to_room_modal div.no_rooms').show();
		} else {
			$('#available_rooms').empty();

			$.each(rooms, function(a, obj) {
    			$("#available_rooms").append('<li>'
    				                         + obj.name
    				                         +' <a class="fa fa-check add_to_room" href="javascript:void(0);" data-user-id="'
    				                         + user_id
    				                         + '" data-room-id="'
    				                         + obj.id
    				                         + '">&nbsp;</a></li>');
			});
		}

	});

	//user added to room, ok.
	//TODO: Maybe, auto join room here?
	socket.on('add_to_room_ok', function(){
		$('#add_user_to_room_modal').modal('hide');
	});


});